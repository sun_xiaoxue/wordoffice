#ifndef MYWORLD_H
#define MYWORLD_H

#include <QMainWindow>

#include <mychild.h>
#include <QtWidgets>
#include <QFontDatabase>

//添加相关头文件
#include <QPrintDialog>
#include <Qprinter>
#include <QPrintPreviewDialog>
#include <QTextCharFormat>

class MyChild; //创建子窗口
class QAction; //被创建后必须将它添加到菜单和工具栏，然后将它链接到实现的action功能的槽函数
class QMenu; //QMenu为菜单，菜单QMenu是挂载在菜单栏QMenuBar（容器）上面的--QMenuBar里面的子集，全都是Qmenu
class QComboBox;//选项列表（组合框）
class QFontComboBox;//字体下拉列表框（不能被编辑，只是用来选择字体）
class QMdiArea; //提供一个可以同时显示多个文档容器的区域
//大多数复杂项目软件，都是使用MDI框架，QMdiArea用于主容器当中，用于容纳多个子容器QMdiSubWindow
class QMdiSubWindow;

class QSignalMapper;//此类专门收集一系列无参信号


namespace Ui {
class Myworld;
}

class Myworld : public QMainWindow
{
    Q_OBJECT

public:
    explicit Myworld(QWidget *parent = 0);
    ~Myworld();


protected:
    //添加事件：系统默认的事件
    void closeEvent(QCloseEvent *event);//通过参数event来控制是否让窗口关闭

private slots: //槽函数
 //   Ui::Myworld *ui;
    void fileNew(); //新建
    void fileOpen(); //打开
    void fileSave(); //保存
    void fileSaveAs(); //另保存
    void filePrint();//文件打印
    void printPreview(QPrinter *printer);
    void filePrintPreview();

    void undo();//撤销
    void redo();//重做
    void cut();//剪切
    void copy();//复制
    void paste();//粘贴
    void about();//关于

    void textBold();
    void textItalic();
    void textUnderline();
    void textAlign(QAction *a);
    void textStyle(int styleIndex);
    void textFamily(const QString &f);
    void textSize(const QString &p);
    void textColor();

    void updateMenus(); //更新菜单
    void updateWindowMenu();
    void setActiveSubWindow(QWidget *window);
    MyChild *createMyChild();


private:
    void createActions();//创建菜单操作
    void createMenus(); //创建菜单
    void createToolBars();//创建工具条
    void createStatusBar();//创建状态条
    void enabledText();
    void fontChanged(const QFont &f);
    void colorChanged(const QColor &c);
    void alignmentChanged(Qt::Alignment a);
    MyChild *activeMyChild();
    QMdiSubWindow *findMyChild(const QString &filename);

    QMdiArea *mdiArea; //多文档容器的容器管理器
    QSignalMapper *windowMapper;

    //定义菜单
    QMenu *fileMenu;
    QMenu *editMenu;
    QMenu *formatMenu;
    QMenu *fontMenu;
    QMenu *alignMenu;
    QMenu *windowMenu;
    QMenu *helpMenu;


    //工具栏
    QToolBar *fileToolBar;
    QToolBar *editToolBar;
    QToolBar *formatToolBar;
    QToolBar *comboToolBar;
    QComboBox *comboStyle;//子控件 标准组合框
    QComboBox *comboFont;//子控件 字体组合框
    QComboBox *comboSize;//子控件 字体大小组合框

    //菜单动作（Action）
    //文本
    QAction *newAct;
    QAction *openAct;
    QAction *saveAct;
    QAction *saveAsAct;
    QAction *printAct;
    QAction *printPreviewAct;
    QAction *exitAct;

    //编辑
    QAction *undoAct;
    QAction *redoAct;
    QAction *cutAct;
    QAction *copyAct;
    QAction *pasteAct;

    //字体
    QAction *boldAct;//加粗
    QAction *italicAct;//斜体
    QAction *underlineAct;//下划线
    QAction *leftAlignAct;
    QAction *centerAct;
    QAction *rightAlignAct;
    QAction *justifyAct;
    QAction *colorAct;

    //窗口
    QAction *closeAct;
    QAction *closeAllAct;
    QAction *tileAct;
    QAction *cascadeAct;
    QAction *nextAct;
    QAction *previousAct;
    QAction *separatorAct;

    QAction *aboutAct;
    //QAction *aboutQtAct;


};

#endif // Myworld_H
