#include "mychild.h"
#include <QTextStream>
#include <QtWidgets>
MyChild::MyChild()
{
    setAttribute(Qt::WA_DeleteOnClose);//关闭窗口时销毁
    isUntitled = true;//标记文件是否被保存过，为真：未保存过；为false则文件已经被保存过
}

void MyChild::newFile() //新建文件
{
    static int sequenceNumber = 1;
    isUntitled=true;
    curFile=tr("Word文档=%1").arg(sequenceNumber++);
    setWindowTitle(curFile);

}

bool MyChild::loadFile(const QString &fileName) //导入文件
{
    if(!fileName.isEmpty())
    {
        if(!QFile::exists(fileName))
            return false;

        QFile file(fileName);
        if (!file.open(QFile::ReadOnly))
            return false;

        QByteArray data=file.readAll();

        QTextCodec *codec=Qt::codecForHtml(data);
        QString str=codec->toUnicode(data);

        if(Qt::mightBeRichText(str)){ //如果是富文件就为HTML
            this -> setHtml(str);
        }else{ //否则不是富文本设置为简单字符串格式
            str=QString::fromLocal8Bit(data);
            this->setPlainText(str);
        }
        setCurrentFile(fileName);
        connect(document(),SIGNAL(contentsChanged()),this,SLOT(documentWasModified()));
    }

}



bool MyChild::maybeSave() //判断是否修改且保存文档
{
    if(!document()->isModified())
        return true;

    QMessageBox::StandardButton ret;
    ret = QMessageBox::warning(this,tr("Qt Word"),tr("文件'%1'已被修改，是否保存？").arg(userFriendlyCurrentFile()),QMessageBox::Save|QMessageBox::Cancel);

    if(ret == QMessageBox::Save)
        return save();
    else if(ret == QMessageBox::Cancel)
        return true;
    return true;

}

bool MyChild::save() //保存文件
{
    if(isUntitled){
        return saveAs();
    }else{
        return saveFile(curFile);
    }
}

bool MyChild::saveAs() //另存为文件
{
    QString fileName=QFileDialog::getSaveFileName(this,tr("另存为"),curFile,tr("HTML 文档(*.html *.html);;所有文件(*.*)"));

    if(fileName.isEmpty())
        return false;

    return saveFile(fileName);

}
bool MyChild::saveFile(QString fileName)
{
    /*
    if(!(fileName.endsWith(".htm",Qt::CaseInsensitive) || fileName.endsWith(".html",Qt::CaseInsensitive)));
    {
        //默认只存为HTML文档
        fileName += ".html";
    }
    */

    QTextDocumentWriter writer(fileName);
    bool success=writer.write(this->document());
    if(success)
        setCurrentFile(fileName);

    return success;
}

void MyChild::documentWasModified()
{
    //在设置改变时，设置窗口已修改（标记已修改）
    setWindowModified(document()->isModified());

}

//格式字体设置
void MyChild::mergeFormationOnWordOrSelection(const QTextCharFormat &format)
{
    QTextCursor cursor=this->textCursor();

    if(!cursor.hasSelection())
        cursor.select(QTextCursor::WordUnderCursor);

    cursor.mergeCharFormat(format);
    this->mergeCurrentCharFormat(format);

}

//段落对齐设置
void MyChild::setAlign(int align)
{
    if(align == 1)
        this->setAlignment(Qt::AlignLeft | Qt::AlignAbsolute); //水平方向靠左对齐、绝对向左对齐
    else if(align == 2)
        this->setAlignment(Qt::AlignCenter); //水平方向居中
    else if(align == 3)
        this->setAlignment(Qt::AlignRight | Qt::AlignAbsolute); //水平方向靠右对齐、绝对向右对齐
    else if(align == 4)
        this->setAlignment(Qt::AlignJustify); //水平方向两边对齐

}

//段落编号
void MyChild::setStyle(int style)
{
    //多行文本框文本光标插入文本
    QTextCursor cursor=this->textCursor();

    if(style != 0){
        QTextListFormat::Style stylename = QTextListFormat::ListDisc; //样式为圆圈

        switch(style){
        deault:
        case 1:
            stylename = QTextListFormat::ListDisc; //样式为圆圈
            break;
        case 2:
            stylename = QTextListFormat::ListCircle; //样式为空心圆圈
            break;
        case 3:
            stylename = QTextListFormat::ListSquare; //样式为方块
            break;
        case 4:
             stylename = QTextListFormat::ListDecimal; //样式为阿拉伯数字
            break;
        case 5:
            stylename = QTextListFormat::ListLowerAlpha; //样式为小写阿拉伯字符 按字母顺序
            break;
        case 6:
            stylename = QTextListFormat::ListUpperAlpha; //样式为大写阿拉伯数字 按字母顺序
            break;
        case 7:
            stylename = QTextListFormat::ListLowerRoman; //样式为小写罗马数字
            break;
        case 8:
            stylename = QTextListFormat::ListUpperRoman; //样式为大写罗马数字
            break;
        }
        cursor.beginEditBlock();
        QTextBlockFormat blockFmt=cursor.blockFormat();
        QTextListFormat listFmt;

        if(cursor.currentList()){
            listFmt = cursor.currentList()->format();
        }else{
            listFmt.setIndent(blockFmt.indent()+1);
            blockFmt.setIndent(0);
            cursor.setBlockFormat(blockFmt);
        }

        listFmt.setStyle(stylename);
        cursor.createList(listFmt);
        cursor.endEditBlock();
    }else{
        QTextBlockFormat bfmt;
        bfmt.setObjectIndex(-1);
        cursor.mergeBlockFormat(bfmt);
    }

}



QString MyChild::strippedName(const QString &fullFileName) //脱离文件名
{
    return QFileInfo(fullFileName).fileName();

}

QString MyChild::userFriendlyCurrentFile()  //用户友好型当前文件
{
    return strippedName(curFile);

}

void MyChild::setCurrentFile(const QString &fileName) //设置当前文件
{
    curFile = QFileInfo(fileName).canonicalFilePath();
    isUntitled=false;
    document()->setModified(false);
    setWindowModified(false);
    setWindowTitle(userFriendlyCurrentFile()+"[*]");

}

void MyChild::closeEvent(QCloseEvent *event)
{
    if(maybeSave()){
        event->accept();
    }else{
        event->ignore();
    }

}


