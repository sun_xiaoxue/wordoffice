#ifndef MYCHILD_H
#define MYCHILD_H

#include <QTextEdit>

class MyChild : public QTextEdit
{
    Q_OBJECT
public:
    MyChild();

    void newFile();  //新建文件
    bool loadFile(const QString &fileName); //导入文件
    bool save(); //保存
    bool saveAs(); //另存为文件
    bool saveFile(QString fileName);//保存文件
    QString userFriendlyCurrentFile();//用户友好型当前文件
    QString currentFile(){ return curFile; }
    void mergeFormationOnWordOrSelection(const QTextCharFormat &format); //格式字体设置
    void setAlign(int align); //对齐方式
    void setStyle(int style);


protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void documentWasModified();


private:
    QString curFile;
    bool isUntitled;

    bool maybeSave();
    void setCurrentFile(const QString &fileName);
    QString strippedName(const QString &fullFileName);

};

#endif // MYCHILD_H
